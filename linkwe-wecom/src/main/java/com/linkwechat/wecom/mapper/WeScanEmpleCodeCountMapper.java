package com.linkwechat.wecom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linkwechat.wecom.domain.WeScanEmpleCodeCount;

public interface WeScanEmpleCodeCountMapper extends BaseMapper<WeScanEmpleCodeCount> {
}
